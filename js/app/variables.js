define(function () {
    var canAnimate = true;
    var activeSlide;
    var totalSlides;
    var animationSpeed;
    var slides;
    var soundsData;
    var menuShown = false;
    var playingMenuTick = false;

    function isMobile() {
        return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
    };

    return {
        canAnimate: canAnimate,
        activeSlide: activeSlide,
        totalSlides: totalSlides,
        animationSpeed: animationSpeed,
        slides: slides,
        soundsData: soundsData,
        menuShown: menuShown,
        playingMenuTick: playingMenuTick,
        isMobile: isMobile
    }
});