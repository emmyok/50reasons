define(['jquery', 'swipe', 'variables', 'slides', 'events', 'menu', 'preloader', 'eventsDispacher'], function ($, swipe, variables, slides, events, menu, preloader, eventsDispacher) {

    function downloadSlides() {
        $.getJSON('data/slides.json', function (data) {
            variables.slides = data;
            variables.totalSlides = data.length;
            preloader.preload();
        });
    };

    function downloadConfig() {
        $.getJSON('data/config.json', function (data) {
            variables.activeSlide = data.activeSlide;
            variables.animationSpeed = data.animationSpeed;
            downloadSlides();
        });
    };

    function downloadSounds() {
        $.getJSON('data/sounds.json', function (data) {
            variables.soundsData = data;
            var music = data.music;
            var audio = new Audio(music);
            audio.volume = 0.1;
            audio.play();
        });
    };

    function imagesReady() {
        slides.createFirstSlide();
        events.detectMouseWheel();
        menu.createMenu();
        menu.playOnMenu();
        menu.showMenu();
    };

    eventsDispacher.on('prealoader finish', imagesReady);
    downloadConfig();
    downloadSounds();
});