define(function () {
    var events = {};

    var on = function () {
        if (!this.events[arguments[0]]) {
            this.events[arguments[0]] = {};
        }
        this.events[arguments[0]][btoa(arguments[1])] = arguments[1];
    };

    var trigger = function () {
        var args = Array.prototype.slice.call(arguments);
        if (this.events[args[0]]) {
            var events = Object.keys(this.events[args[0]]);

            for (var i = 0; i < events.length; i++) {
                if (this.events[args[0]][events[i]]) {
                    this.events[args[0]][events[i]](args.length > 1 ? args.pop() : null);
                }
            }
        }
    };

    var off = function () {
        this.events[arguments[0]][btoa(arguments[1])] = null;
        delete(this.events[arguments[0]][btoa(arguments[1])]);
        this.events[arguments[0]] = null;
        delete(this.events[arguments[0]]);
    };

    return {
        events: events,
        on: on,
        trigger: trigger,
        off: off
    }
});