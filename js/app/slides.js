define(['variables', 'menu', 'eventsDispacher'], function (variables, menu, eventsDispacher) {
    function createFirstSlide() {

        $('body').append('<div id="slide' + variables.activeSlide + '" class="slide">' +
            '<div class="background" style="background-image: url(' + variables.slides[variables.activeSlide - 1].background + ')"></div>' +
            '<div class="page-content start-content">' +
            '<div class="destination">' +
            '<div class="country">' + variables.slides[variables.activeSlide - 1].country + '</div>' +
            '<div class="title">' + variables.slides[variables.activeSlide - 1].title + '</div>' +
            '</div>' +
            '<div class="content">' +
            '<div class="info"><img src="./assets/images/plus.svg"/><p>Read more</p>' + '</div>' +
            '<div class="horizLine"></div>' +
            '<div class="numberss">' +
            '<div class="actiSlid">' + variables.activeSlide.valueOf() + '</div>' +
            '<div class="verticalLine"></div>' +
            '<div class="totalSlide">' + variables.totalSlides + '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="txShow" style="background-image: url(' + variables.slides[variables.activeSlide].background + ')">' + (variables.activeSlide + 1) + '</div>' +
            '</div>')
    }

    function showNextSlide() {
        if (variables.canAnimate && variables.activeSlide < variables.totalSlides) {
            variables.canAnimate = false;

            animateContent();
            addAndAnimateNumber(1);
            animatebackground(1, '150px', $(window).height(), 1);
        }

    }

    function showPreviousSlide() {
        if (variables.canAnimate && variables.activeSlide > 1) {
            variables.canAnimate = false;

            animateContent();
            addAndAnimateNumber(-1);
            animatebackground(-1, '-150px', -$(window).height(), 1);
        }

    }
    function animateContent() {
        $('.page-content').animate({
            opacity: 0
        }, variables.animationSpeed);
    }

    function addAndAnimateNumber(a) {
        $('.txShow').html(variables.activeSlide + a);
        $('.txShow').css({
            marginTop: (-$('.txShow').height() / 2) + a * 50,
            backgroundImage: 'url(' + variables.slides[variables.activeSlide + a - 1].background + ')'
        });

        setTimeout(function () {
            $('.txShow').animate({
                'margin-top': -$('.txShow').height() / 2,
                opacity: 1
            })
        }, 300);
    }

    function animatebackground(a, b, c, d) {
        $('video').animate({
            opacity: '0.2'
        }, variables.animationSpeed);
        $('.background').animate({
            opacity: '0.2'
        }, variables.animationSpeed, function () {
            $('video').remove();
            createSlide(a, b, c, d);
            animateSlide();
        })
    }

    function createSlide(a, b, c, d) {

        variables.activeSlide = variables.activeSlide + a;

        menu.makeActiveMenuSlide();

        $('.slide').after('<div id="slide' + variables.activeSlide + '" class="slide" style="top:' + c + 'px">' +
            '<div class="background" style="background-image: url(' + variables.slides[variables.activeSlide - d].background + ')"></div>' +
            '<video src="' + (variables.slides[variables.activeSlide - d].video ? variables.slides[variables.activeSlide - d].video : '') + '" autoplay="" loop=""></video>' +
            '<div class="page-content" style="top:' + b + '">' +
            '<div class="destination">' +
            '<div class="country" >' + variables.slides[variables.activeSlide - d].country + '</div>' +
            '<div class="title">' + variables.slides[variables.activeSlide - d].title + '</div>' +
            '</div>' +
            '<div class="content">' +
            '<div class="info"><img src="./assets/images/plus.svg"/><p>Read more</p>' + '</div>' +
            '<div class="horizLine"></div>' +
            '<div class="numberss">' +
            '<div class="actiSlid">' + variables.activeSlide.valueOf() + '</div>' +
            '<div class="verticalLine"></div>' +
            '<div class="totalSlide">' + variables.totalSlides + '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="txShow"></div>' +
            '</div>')
    }

    function animateSlide() {
        playOnSlidesTrans()
        if (variables.isMobile()) {
            $('#slide' + variables.activeSlide).css({
                top: '0px'
            })
            afterAnimation()
        } else {
            $('#slide' + variables.activeSlide).animate({
                top: '0px'
            }, variables.animationSpeed, function () {
                afterAnimation();
            })
        }
    }

    function afterAnimation() {
        $('.slide').first().remove();

        $('.txShow').animate({
            opacity: 0
        }, variables.animationSpeed);

        variables.canAnimate = true;

        $('.page-content').animate({
            opacity: 1,
            top: 0
        }, variables.animationSpeed);
    }

    function playOnSlidesTrans() {
        var SlideMusic = variables.soundsData.slideChange;
        var audio = new Audio(SlideMusic);
        audio.play();
    }

    eventsDispacher.on('slides.showNextSlide', showNextSlide);

    return {
        createFirstSlide: createFirstSlide,
        showNextSlide: showNextSlide,
        showPreviousSlide: showPreviousSlide
    }
});