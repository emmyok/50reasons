define(['slides', 'variables'], function (slides, variables) {
    function detectMouseWheel() {
        $(window).bind('mousewheel DOMMouseScroll', function (event) {
            if (!variables.menuShown) {
                if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
                    slides.showPreviousSlide();
                }
                else {
                    slides.showNextSlide();
                }
            }
        });

        $('body').swipe({
            swipeUp: function () {
                slides.showNextSlide();
            },
            swipeDown: function () {
                slides.showPreviousSlide();
            },
            threshold: 100,
            allowPageScroll: 'vertical'
        });
    }

    return {
        detectMouseWheel: detectMouseWheel
    };
});