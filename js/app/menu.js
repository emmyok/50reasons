define(['variables', 'eventsDispacher'], function (variables, eventsDispacher) {
    function makeActiveMenuSlide() {
        $('body').find('.active').removeClass('active');
        $('#menuSlide' + variables.activeSlide).addClass('active');
    };

    function createMenu() {
        for (var x = 0; x < variables.slides.length; x++) {
            $('#menu').append('<div class="menuSlide" id="menuSlide' + (x + 1) + '" data-id="' + (x) + '" ><div class="menuBackground"  style="background-image: url(' + variables.slides[x].background + ')"></div> <div class="horLin"></div><div class="numberShow">' + (x + 1) + '</div> <div class="menuTitle">' + variables.slides[x].title + '</div></div>');
        }
        makeActiveMenuSlide();
        playOnMenuSlides();
        clickMenuSlide();
    };

    function playOnMenuSlides() {
        $('.menuSlide').hover(function () {
            if (!variables.playingMenuTick) {
                variables.playingMenuTick = true;
                var bttnMusic = variables.soundsData.menutick;
                var soundMenuTick = new Audio(bttnMusic);
                soundMenuTick.play();
                setTimeout(function () {
                    variables.playingMenuTick = false;
                }, 200)
            }
        })
    };

    function clickMenuSlide() {
        $('.menuSlide').click(function () {
            window.scrollTo(0, 0);
            if (parseInt($(this).attr('data-id')) !== variables.activeSlide - 1) {
                variables.activeSlide = parseInt($(this).attr('data-id'));
                hideMenu();
                eventsDispacher.trigger('slides.showNextSlide');
            }
        })
    };

    function hideMenu() {
        $('body').removeClass('activeMenu');
        variables.menuShown = false;
        $('#menu').css({visibility: 'hidden'});
    };

    function playOnMenu() {
        $('#bttn').hover(function () {
            var bttnMusic = variables.soundsData.button1;
            var audio = new Audio(bttnMusic);
            audio.play();
        }, function () {
        })
    };

    function showMenu() {
        $('#bttn').click(function () {
            if (variables.menuShown) {
                hideMenu();
            } else {
                $('body').addClass('activeMenu');
                variables.menuShown = true;
                $('#menu').css({visibility: 'visible'});
            }
            ;
        });
    };

    return {
        makeActiveMenuSlide: makeActiveMenuSlide,
        createMenu: createMenu,
        playOnMenu: playOnMenu,
        showMenu: showMenu
    };
});