define(['variables', 'eventsDispacher'], function (variables, eventsDispacher) {

    function preload() {
        var howManyImagesDownloaded = 0;
        $(variables.slides).each(function () {
            var downloadingImage = new Image();
            downloadingImage.onload = function () {
                howManyImagesDownloaded++;
                if (howManyImagesDownloaded < 10) {
                    $('.loaderNumbers').html('0' + howManyImagesDownloaded);
                } else {
                    $('.loaderNumbers').html(howManyImagesDownloaded);
                }

                if (!variables.playingMenuTick) {
                    variables.playingMenuTick = true;
                    var audio = new Audio(variables.soundsData.menutick);
                    audio.play();
                    setTimeout(function () {
                        variables.playingMenuTick = false
                    }, 200);
                }

                if (howManyImagesDownloaded === variables.totalSlides) {
                    eventsDispacher.trigger('prealoader finish');
                    $('.preloader').remove();
                }
            };
            downloadingImage.src = this.background;
        });
    };

    return {
        preload: preload
    }
})