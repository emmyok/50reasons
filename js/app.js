requirejs.config({
    baseUrl: 'js/app',
    paths: {
        app: "../app",
        jquery: '../lib/jquery.min',
        swipe: '../lib/jquery.touchSwipe.min'
    }
});

requirejs(["app/main"]);